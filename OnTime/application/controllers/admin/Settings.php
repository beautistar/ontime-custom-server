<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Settings extends MY_Controller {

        public function __construct(){
            parent::__construct();
            $this->load->model('admin/setting_model', 'setting_model');
        }

        public function index(){
            
            $data['cur_duration'] = $this->setting_model->get_cur_duration();
            $data['title'] = 'Setting';
            $data['view'] = 'admin/settings/duration';
            $this->load->view('admin/layout', $data);
        }
        
        //---------------------------------------------------------------
        //  updae duration
        public function update_duration(){
            if($this->input->post('submit')){

                $this->form_validation->set_rules('duration', 'Frequency', 'trim|required'); 

                if ($this->form_validation->run() == FALSE) {
                    $data['cur_duration'] = $this->setting_model->get_cur_duration();
                    $data['title'] = 'Setting';
                    $data['view'] = 'admin/settings/duration';
                    $this->load->view('admin/layout', $data);
                }
                else{
                    $data = array(
                        'duration' => $this->input->post('duration'),
                        'updated_at' => date('Y-m-d h:m:s')
                    );
                    $data = $this->security->xss_clean($data);
                    $result = $this->setting_model->update_duration($data);
                    if($result){
                        $this->session->set_flashdata('msg', 'Frequency has been updated Successfully!');
                        redirect(base_url('admin/settings'));
                    }
                }
            }
            else {
                $data['cur_duration'] = $this->setting_model->get_cur_duration();
                $data['title'] = 'Setting';
                $data['view'] = 'admin/settings/duration';
                $this->load->view('admin/layout', $data);
            }
            
        }
                
    }


?>