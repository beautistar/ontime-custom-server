<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Schools extends MY_Controller {

        public function __construct(){
            parent::__construct();
            $this->load->model('admin/school_model', 'school_model');
            $this->load->model('app/api_model', 'api_model');
        }

        //----------------------------------------------------------------------
        //  Schools
        public function index(){
            $data['all_schools'] =  $this->school_model->get_all_schools();
            $data['state'] = $this->api_model->getState();
            $data['title'] = 'Schools';
            $data['view'] = 'admin/schools/school_list';
            $this->load->view('admin/layout', $data);
        }
        
        
        //----------------------------------------------------------------------
        //  school delete
        public function del_school($id = 0){
            $this->db->delete('tb_school', array('id' => $id));
            $this->session->set_flashdata('msg', 'School has been Deleted Successfully!');
            redirect(base_url('admin/schools'));
        }
        
        public function ajax_call(){
			 $id_state = $this->input->post('state'); 
                  $output="";
                  if($id_state){
                      $arrcountry['data'] = $this->api_model->getCounty($id_state); 
                      foreach ($arrcountry['data'] as $row){  
                          ?>
                          <option value=<?php echo $row['id'];?>><?php echo $row['name'];?></option>  
                          <?php   
                      }
                  } else {
                       redirect(base_url('home'));
                  }
	    }
		public function ajax_county(){
			 $county = $this->input->post('county'); 
             $output="";
             if($county){
                 $arrtoenship['data'] = $this->api_model->getTownship($county); 
                     foreach ($arrtoenship['data'] as $row) {
                         ?><option value=<?php echo $row['id'];?>><?php echo $row['name'];?></option>  
                         <?php   
                     }
             } else {
                  redirect(base_url('home'));
             }
        }

        
        //---------------------------------------------------------------
        //  Add School
        public function add(){
            
            $data['state'] = $this->api_model->getState();
            $data['title'] = 'Add School';
            $data['view'] = 'admin/schools/school_add';
            $this->load->view('admin/layout', $data);            
        }

        public function add_scchool(){
			if(isset($_POST)){
				//print_r($_POST);
				extract($_POST); 
				
                $data = $this->getLatLong($address);
                
                if ($data == false) {
                    $this->session->set_flashdata('msg', 'Unknown address!'); 
                    redirect(base_url('admin/schools/add'));                                        
                } else {
                    $data['township_id']=$township;
                    $data['name']=$name;
                    $data['address']=$address;                    
                    $result = $this->school_model->add_school($data);
                    $this->session->set_flashdata('msg', 'School has been Added Successfully!');                        
                    redirect(base_url('admin/schools'));                    
                } 				
			}
		}
        
        function getLatLong($address){
            if(!empty($address)){
                //Formatted address
                $formattedAddr = str_replace(' ','+',$address);
                //Send request and receive json data by address
                $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false&key=AIzaSyDE1gBhG8sZKx9oNZ8QQDj5cedZrNjBSB4'); 
                $output = json_decode($geocodeFromAddr);
                //Get latitude and longitute from json data
                //print_r($output);
                if (empty($output->results)) {
                    return false;
                }
                $data['latitude']  = $output->results[0]->geometry->location->lat?$output->results[0]->geometry->location->lat:""; 
                $data['longitude'] = $output->results[0]->geometry->location->lng?$output->results[0]->geometry->location->lng:"";
                //Return latitude and longitude of the given address
                if(!empty($data)){
                    return $data;
                }else{
                    return false;
                }
            }else{
                return false;   
            }
        }
        
        public function filter_school(){
            if(isset($_POST)){
                
                extract($_POST); 
                $this->form_validation->set_rules('township', 'Options', 'trim|required');
                
                if ($this->form_validation->run() == FALSE) {
                    $data['all_schools'] =  $this->school_model->get_all_schools();
                    $data['state'] = $this->api_model->getState();
                    $data['title'] = 'Schools';
                    $data['view'] = 'admin/schools/school_list';
                    $this->load->view('admin/layout', $data);
                } else {
                    $data['township_id']=$township;
                    if (isset($township) && $township != 0) {
                       $data['all_schools'] =  $this->school_model->get_filter_schools($township); 
                    } else {
                       $data['all_schools'] =  $this->school_model->get_all_schools();  
                    }                    
                    
                    $data['state'] = $this->api_model->getState();
                    $data['title'] = 'Schools';
                    $data['view'] = 'admin/schools/school_list';
                    $this->load->view('admin/layout', $data);                
                    
                }                
            }
        }
    }


?>
