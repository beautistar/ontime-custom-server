<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Bus extends MY_Controller {

        public function __construct(){
            parent::__construct();
            $this->load->model('admin/bus_model', 'bus_model');
            $this->load->model('app/api_model', 'api_model');
        }

        //----------------------------------------------------------------------
        //  Bus
        public function index(){
            $data['all_bus'] =  $this->bus_model->get_all_bus();
            $data['state'] = $this->api_model->getState();
            $data['title'] = 'Bus';
            $data['view'] = 'admin/bus/bus_list';
            $this->load->view('admin/layout', $data);
        }
        
        //----------------------------------------------------------------------
        //  Bus delete
        public function del_bus($id = 0){
            $this->db->delete('tb_bus', array('id' => $id));
            $this->session->set_flashdata('msg', 'Bus has been Deleted Successfully!');
            redirect(base_url('admin/bus'));
        }        
        
        
        //---------------------------------------------------------------
        //  Add Bus
        public function add(){
            $data['state'] = $this->api_model->getState();
            $data['user_groups'] = array();
            $data['title'] = 'Add Bus';
            $data['view'] = 'admin/bus/bus_add';
            $this->load->view('admin/layout', $data);
          
            
        }
        
        public function add_bus(){
            if(isset($_POST)){
                //print_r($_POST);
                extract($_POST);

                $data['school_id']=$school;
                $data['number']=$bus_no;
                $result = $this->bus_model->add_bus($data);
                $this->session->set_flashdata('msg', 'Bus has been Added Successfully!');                        
                redirect(base_url('admin/bus'));                    
                                 
            }
        }
        
        //----------------------------------------------------------------------
        //  Ajax function.
        public function ajax_call(){
             $id_state = $this->input->post('state'); 
                  $output="";
                  if($id_state){
                      $arrcountry['data'] = $this->api_model->getCounty($id_state); 
                      foreach ($arrcountry['data'] as $row){  
                          ?>
                          <option value=<?php echo $row['id'];?>><?php echo $row['name'];?></option>  
                          <?php   
                      }
                  } else {
                       redirect(base_url('home'));
                  }
        }
        public function ajax_county(){
             $county = $this->input->post('county'); 
             $output="";
             if($county){
                 $arrtoenship['data'] = $this->api_model->getTownship($county); 
                     foreach ($arrtoenship['data'] as $row) {
                         ?><option value=<?php echo $row['id'];?>><?php echo $row['name'];?></option>  
                         <?php   
                     }
             } else {
                  redirect(base_url('home'));
             }
        }
        
        public function ajax_township(){
             $township = $this->input->post('township'); 
             $output="";
             if($township){
                 $arrschool['data'] = $this->api_model->getSchool($township); 
                     foreach ($arrschool['data'] as $row) {
                         ?><option value=<?php echo $row['id'];?>><?php echo $row['name'];?></option>  
                         <?php   
                     }
             } else {
                  redirect(base_url('home'));
             }
        }
        
        public function filter_bus(){
            if(isset($_POST)){
                
                extract($_POST); 
                $this->form_validation->set_rules('school', 'Options', 'trim|required');
                
                if ($this->form_validation->run() == FALSE) {
                    $data['all_bus'] =  $this->bus_model->get_all_bus();
                    $data['state'] = $this->api_model->getState();
                    $data['title'] = 'Bus';
                    $data['view'] = 'admin/bus/bus_list';
                    $this->load->view('admin/layout', $data);
                } else {
                    $data['school_id']=$school;
                    if (isset($school) && $school != 0) {
                       $data['all_bus'] =  $this->bus_model->get_filter_bus($school); 
                    } else {
                       $data['all_bus'] =  $this->bus_model->get_all_bus();  
                    }                    
                    
                    $data['state'] = $this->api_model->getState();
                    $data['title'] = 'Bus';
                    $data['view'] = 'admin/bus/bus_list';
                    $this->load->view('admin/layout', $data);
                }                
            }
        }        
    }


?>