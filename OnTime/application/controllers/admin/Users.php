<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Users extends MY_Controller {

		public function __construct(){
			parent::__construct();
            $this->load->model('admin/user_model', 'user_model');
			$this->load->model('app/api_model', 'api_model');
		}

        //----------------------------------------------------------------------
        //  Students
		public function index(){
			$data['all_users'] =  $this->user_model->get_all_students();
            $data['state'] = $this->api_model->getState();
			$data['title'] = 'Students';
			$data['view'] = 'admin/users/student_list';
			$this->load->view('admin/layout', $data);
		}
        
        //----------------------------------------------------------------------
        //  Student enable/disable setting
        public function studnet_enable($id = 0) {
            
            $this->user_model->student_enable($id); 
            $this->session->set_flashdata('msg', 'Student status has been updated Successfully!');
            redirect(base_url('admin/users'));        
            
        }
        
        //----------------------------------------------------------------------
        //  Student delete
        public function student_del($id = 0){
            $this->db->delete('tb_student', array('id' => $id));
            $this->session->set_flashdata('msg', 'Student has been Deleted Successfully!');
            redirect(base_url('admin/users'));
        }        
        
        //----------------------------------------------------------------------
        //  Drivers
        public function driver(){
            $data['all_users'] =  $this->user_model->get_all_drivers();
            $data['state'] = $this->api_model->getState();
            $data['title'] = 'Drivers';
            $data['view'] = 'admin/users/driver_list';
            $this->load->view('admin/layout', $data);
        }
		
        //----------------------------------------------------------------------
        //  Driver enable/disable setting
        public function driver_enable($id = 0) {
            
            $this->user_model->driver_enable($id); 
            $this->session->set_flashdata('msg', 'Driver status has been updated Successfully!');
            redirect(base_url('admin/users/driver'));        
            
        }
        
        //----------------------------------------------------------------------
        //  Driver delete
        public function driver_del($id = 0){
            $this->db->delete('tb_driver', array('id' => $id));
            $this->session->set_flashdata('msg', 'Driver has been Deleted Successfully!');
            redirect(base_url('admin/users/driver'));
        }
        
        //----------------------------------------------------------------------
        //  Driver approve
        public function driver_approve($id = 0){
            $this->user_model->driver_approve($id); 
            $this->session->set_flashdata('msg', 'Driver has been approved Successfully!');
            redirect(base_url('admin/users/driver'));
        }
        
        //----------------------------------------------------------------------
        // Filter driver
        public function filter_driver(){
            if(isset($_POST)){
                
                extract($_POST); 
                $this->form_validation->set_rules('township', 'Options', 'trim|required');
                
                if ($this->form_validation->run() == FALSE) {
                    $data['all_users'] =  $this->user_model->get_all_drivers();
                    $data['state'] = $this->api_model->getState();
                    $data['title'] = 'Drivers';
                    $data['view'] = 'admin/users/driver_list';
                    $this->load->view('admin/layout', $data);
                } else {
                    $data['school_id']=$school;
                    if (isset($school) && $school != 0) {
                       $data['all_users'] =  $this->user_model->get_filter_drivers($school); 
                    } else {
                       $data['all_users'] =  $this->user_model->get_all_drivers();  
                    }                    
                    
                    $data['state'] = $this->api_model->getState();
                    $data['title'] = 'Drivers';
                    $data['view'] = 'admin/users/driver_list';
                    $this->load->view('admin/layout', $data);                
                    
                }                
            }
        }
        
        //----------------------------------------------------------------------
        // Filter driver
        public function filter_student(){
            if(isset($_POST)){
                
                extract($_POST); 
                $this->form_validation->set_rules('school', 'Options', 'trim|required');
                
                if ($this->form_validation->run() == FALSE) {
                    $data['all_users'] =  $this->user_model->get_all_students();
                    $data['state'] = $this->api_model->getState();
                    $data['title'] = 'Students';
                    $data['view'] = 'admin/users/student_list';
                    $this->load->view('admin/layout', $data);
                } else {
                    $data['school_id']=$school;
                    if (isset($school) && $school != 0) {
                       $data['all_users'] =  $this->user_model->get_filter_student($school); 
                    } else {
                       $data['all_users'] =  $this->user_model->get_all_students();  
                    }                    
                    
                    $data['state'] = $this->api_model->getState();
                    $data['title'] = 'Students';
                    $data['view'] = 'admin/users/student_list';
                    $this->load->view('admin/layout', $data);                
                    
                }                
            }
        }
        
        
	}


?>