<?php
	class User_model extends CI_Model{

		public function add_user($data){
			$this->db->insert('ci_users', $data);
			return true;
		}

		public function get_all_students(){
			$query = $this->db->get('tb_student');
			return $result = $query->result_array();
		}
        
        public function get_all_drivers(){
            $query = $this->db->get('tb_driver');
            return $result = $query->result_array();
        }         
        
        function student_enable($id) {
            
            $this->db->set('is_enabled', 'CASE WHEN `is_enabled` = 1 THEN 0 ELSE 1 END', FALSE)
                    ->where('id', $id)
                    ->update('tb_student');
        }        
        
        function driver_enable($id) {
            
            $this->db->set('is_enabled', 'CASE WHEN `is_enabled` = 1 THEN 0 ELSE 1 END', FALSE)
                    ->where('id', $id)
                    ->update('tb_driver');
        }
        
        function driver_approve($id) {
            
            $this->db->set('is_approved', 1)
                     ->where('id', $id)
                     ->update('tb_driver');
            
        }		
		
        public function get_filter_drivers($school) {
            
            $this->db->where('school_id', $school);
            return $this->db->get('tb_driver')->result_array();
        }
        
        public function get_filter_student($school) {
            
            $this->db->where('school_id', $school);
            return $this->db->get('tb_student')->result_array();
        }
	}

?>