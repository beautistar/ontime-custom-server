<?php
    class School_model extends CI_Model{

        
        public function get_all_schools() {
            
            return $this->db->get('tb_school')->result_array();
            
        }
        
        public function add_school($data){
            $this->db->insert('tb_school', $data);
            return true;
        } 
        
        public function get_filter_schools($township) {
            $this->db->where('township_id', $township);
            return $this->db->get('tb_school')->result_array();
        }       
    }

?>