<?php
    class Bus_model extends CI_Model{

        
        public function get_all_bus() {
            
            return $this->db->select('tb_bus.*, tb_school.name, tb_school.address')
                            ->from('tb_bus')
                            ->join('tb_school', 'tb_bus.school_id = tb_school.id')
                            ->get()
                            ->result_array();
            
        }
        
        public function add_bus($data) {
            
            $this->db->insert('tb_bus', $data);
            return true;
        }
        
        function get_filter_bus($school) {
            
            return $this->db->select('tb_bus.*, tb_school.name, tb_school.address')
                            ->from('tb_bus')
                            ->where('school_id', $school)
                            ->join('tb_school', 'tb_bus.school_id = tb_school.id')
                            ->get()
                            ->result_array();
        }        
    }

?>