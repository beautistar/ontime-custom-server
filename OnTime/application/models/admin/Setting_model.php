<?php
    class Setting_model extends CI_Model{

        public function get_cur_duration(){
            return $this->db->get('tb_setting')->row_array();             
        }        
        
        public function update_duration($data) {
        
            if ($this->db->get('tb_setting')->num_rows() == 0) {                
                $this->db->insert('tb_setting', $data);                
            } else {                
                $this->db->update('tb_setting', $data);
            }
            
            return true;            
        }
    }

?>