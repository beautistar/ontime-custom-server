<style>
.help-block{
    visibility: visible;
    width: auto;
    background-color:  #dc4e41 ;
    color: #fff !important;
    text-align: center;
    border-radius: 3px;
    position: absolute;
    z-index: 5;
    font-size: 12px;
    padding: 0px 15px;
    line-height: 20px;
    vertical-align: middle;
        margin-top: -0px;
}


.help-block::before {
    content: "";
    position: absolute;
    left: 15px;
    top: -5px;
    font-size: 20px;
    color: #fc0e2f;
    z-index: 5;
    vertical-align: top;
    border-bottom: 6px solid #dc4e41;
    border-right: 6px solid transparent;
    border-left: 6px solid transparent;
}

.error{
  color: #dc4e41;
    font-size: 12px;
} 
</style>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body with-border">
        <div class="col-md-6">
          <h4><i class="fa fa-building"></i> &nbsp; Add New School</h4>
        </div>
        <div class="col-md-6 text-right">
          <a href="<?= base_url('admin/schools'); ?>" class="btn btn-success"><i class="fa fa-list"></i> School List</a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php //echo form_open(base_url('admin/schools/add_scchool'), 'class="form-horizontal"');  ?> 
            <form action="<?php echo base_url('admin/schools/add_scchool')?>" id="school" method="post" class="form-horizontal">
              <div class="form-group">
                <label for="state" class="col-sm-2 control-label">State</label>

                <div class="col-sm-9">
                  <!--<input type="text" name="state" class="form-control" id="state" placeholder="">-->
                  <select name="state" id="state" class="form-control">
                    <option value="" >Select State</option>
                      <?php foreach($state as $coun){
                        echo '<option value="'.$coun['id'].'" id="itemone">'.$coun['name'].'</option>';
                        }
                      ?> 
                  </select><p id="ad_state"></p>
                </div>
              </div>
              <div class="form-group">
                <label for="county" class="col-sm-2 control-label">County</label>

                <div class="col-sm-9">
                  <select name="county" class="form-control" id="county" placeholder="">
                    <option value="" >Select County</option>
                  </select>
                  <p id="ad_county"></p>
                </div>
              </div>
              
              <div class="form-group">
                <label for="township" class="col-sm-2 control-label">Township</label>

                <div class="col-sm-9">
                  <select name="township" class="form-control" id="township" placeholder="">
                    <option value="" >Select township</option>
                  </select>
                  <p id="ad_township"></p>
                </div>
              </div>

              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">School Name</label>

                <div class="col-sm-9">
                  <input type="text" name="name" class="form-control" id="name" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="address" class="col-sm-2 control-label">Address</label>

                <div class="col-sm-9">
                  <input type="text" name="address" class="form-control" id="address" placeholder="">
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-11">
                  <input type="submit" name="submit" id="submitsuppply" value="Add School" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script type="text/javascript">
  $(document).ready(function () { 
        
      $('#state').change(function () {
            
          var state = $(this).val();
          $("#ad_county").html("");
          $.ajax({   
                url: "<?php echo base_url('admin/schools/ajax_call')?>", 
                type: "POST", 
                data: "state="+state, 
                success: function(html) 
                {
                    $('#county').html(html);
                }
          })
      });
    
      $('#county').change(function () {
         
         $("#ad_state").html("");
         var county = $(this).val();
         $.ajax({   
                url: "<?php echo base_url('admin/schools/ajax_county')?>", 
                type: "POST", 
                data: "county="+county, 
                success: function(html) 
                {
                   $('#township').html(html);
                }
         })
      });
  });

  $(document).ready(function(){
      
      $("#school").validate({
        debug: false,
        errorClass: "help-block",
        errorElement: "span",
        rules: {
            name: {
            required: true,
            },
        
            address: {
                required: true,
            },
        },
        messages: {          
            name: {
                required: "Enter  Name",
            },
          
            address:{
                required: "Enter address",
            }          
        },
        
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
       
        submitHandler: function(form) {

            if(validateFormad())
                {   
                $("#submitsuppply").prop("disabled", "disabled");
                    form.submit(); 
                    return true;
                }else{
                    return false;
                }
        }  
   });
});

function validateFormad(){
    
  state=document.getElementById("state").value; 
  if(state==0){    

    $("#ad_state").html('<p  style="display: block;" class="help-block">Please Select State</p>');
        return false;
    }
  
  county =  document.getElementById("county").value;  
  if(county==0){    
    $("#ad_county").html('<p  style="display: block;" class="help-block">Please Select Country</p>');
        return false;
    }
  
  township=document.getElementById("township").value;
        if(township==0)
         {    
         $("#ad_township").html('<p  style="display: block;" class="help-block">Please Select City</p>');
        return false;
  }
  
  return true;
    
}
</script>
