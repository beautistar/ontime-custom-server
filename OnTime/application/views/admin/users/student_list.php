<!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css"> 
  

 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h3><i class="fa fa-list"></i> &nbsp; Student List</h3>
        </div>
        <!--<div class="col-md-6 text-right">
          <div class="btn-group margin-bottom-20"> 
            <a href="<?= base_url('admin/users/create_users_pdf'); ?>" class="btn btn-success">Export as PDF</a>
            <a href="<?= base_url('admin/users/export_csv'); ?>" class="btn btn-success">Export as CSV</a>
          </div>
        </div>-->
        
      </div>
    </div>
  </div>
  
  <div class="box border-top-solid border-success">
    <!-- /.box-header -->
    <?php echo form_open(base_url('admin/users/filter_student'), 'class="form-horizontal"');  ?> 
            
    <div class="box-body ">
      <div class="form-group">
        <div class="col-md-2 ">
          <select name="state" id="state" class="form-control">
            <option value="" >Select State</option>
              <?php foreach($state as $coun){
                echo '<option value="'.$coun['id'].'" id="itemone">'.$coun['name'].'</option>';
                }
              ?> 
          </select>
        </div>  
        <div class="col-md-3">
          <select name="county" class="form-control" id="county" placeholder="">
            <option value="" >Select County</option>
          </select>
        </div>
        <div class="col-md-3">
          <select name="township" class="form-control" id="township" placeholder="">
            <option value="" >Select township</option>
          </select>
        </div>
        <div class="col-md-3">
          <select name="school" class="form-control" id="school" placeholder="">
            <option value="" >Select School</option>
          </select>
        </div>
        <div class="col-md-1">
          <input type="submit" name="submit" id="submitsuppply" value="Filter" class="btn btn-success pull-right col-md-12">
        </div>
      </div>
    </div>
    <?php echo form_close( ); ?>
    <!-- /.box-body -->
  </div>
  
   <div class="box border-top-solid">
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
          <th>Photo</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
          <th>Phone NO</th>
          <th>School Name</th>
          <th>Bus NO</th>
          <th>Created at:</th>   
          <th style="width: 100px;" class="text-right">Action</th>
        </tr>
        </thead>
        <tbody>
          <?php foreach($all_users as $row): ?>
          <tr>
            <td ><img src="<?= $row['photo_url']; ?>" style="background-color: gray; width: 50px; height: 50px;"  ></td>
            <td><?= $row['first_name']; ?></td> 
            <td><?= $row['last_name']; ?></td>
            <td><?= $row['email']; ?></td>
            <td><?= $row['phone_number']; ?></td>
            <td><?= $row['school_name']; ?></td>
            <td><?= $row['bus_no']; ?></td>
            <td><?= $row['created_at']; ?></td> 
            <td class="text-right">
                <a href="<?= base_url('admin/users/studnet_enable/'.$row['id']); ?>" 
                <?= ($row['is_enabled'] == 1)? 'class="btn btn-success btn-flat btn-xs"': 'class="btn btn-info btn-flat btn-xs"' ?>>
                <?= ($row['is_enabled'] == 1)? 'Enable': 'Disable' ?></a>
                <a data-href="<?= base_url('admin/users/student_del/'.$row['id']); ?>" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#confirm-delete">Delete</a>
            </td>
            
          </tr>
          <?php endforeach; ?>
        </tbody>
       
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>  


<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Dialog</h4>
      </div>
      <div class="modal-body">
        <p>As you sure you want to delete.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>

  </div>
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script> 
  <script type="text/javascript">
      $('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
  </script>
  
<script>
$("#view_users").addClass('active');
</script>   



<script type="text/javascript">
$(document).ready(function () { 
        
  $('#state').change(function () {
        
      var state = $(this).val();          
      $.ajax({   
            url: "<?php echo base_url('admin/bus/ajax_call')?>", 
            type: "POST", 
            data: "state="+state, 
            success: function(html) 
            {
                $('#county').html(html);
            }
      })
  });

  $('#county').change(function () {         
     
     var county = $(this).val();
     $.ajax({   
            url: "<?php echo base_url('admin/bus/ajax_county')?>", 
            type: "POST", 
            data: "county="+county, 
            success: function(html) 
            {
               $('#township').html(html);
            }
     })
  });
  
  $('#township').change(function () {        
        var township = $(this).val();        
            $.ajax({   
                url: "<?php echo base_url('admin/bus/ajax_township')?>", 
                type: "POST", 
                data: "township="+township, 
                success: function(html){
                   $('#school').html(html);
                }
            })
     });
     
});
</script> 