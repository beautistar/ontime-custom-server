<link rel="stylesheet" href="<?= base_url() ?>public/custom_css/upload_button.css">
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body with-border">
        <div class="col-md-6">
          <h3><i class="fa fa fa-cog"></i> &nbsp; Refresh Frequency</h3>
        </div>        
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open_multipart(base_url('admin/settings/update_duration/'), 'class="form-horizontal"');  ?> 
              
              <div class="form-group">
                <label for="cur_duration" class="col-sm-3 control-label">Current Frequency (s)</label>

                <div class="col-sm-9">
                  <input type="number" name="cur_duration" disabled class="form-control" id="cur_price" placeholder="" value=<?= $cur_duration['duration']; ?> >
                </div>
              </div>              
              
              <div class="form-group">
                <label for="created_at" class="col-sm-3 control-label">Updated date</label>

                <div class="col-sm-9">
                  <input type="text" name="created_at" disabled class="form-control" id="created_at" placeholder="" value=<?= $cur_duration['updated_at']; ?> >
                </div>
              </div> 
              
              <div class="form-group">
                <label for="created_at" class="col-sm-3 control-label">Update Frequency (s)</label>

                <div class="col-sm-9">
                  <abbr title="Frequency value is second.">  
                  <input type="text" name="duration" class="form-control" id="duration" placeholder="New frequency here..." >
                  </abbr>
                </div>
              </div>                                        
  
              <div class="form-group">
                <div class="col-md-12">
                  <input type="submit" name="submit" value="Update" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 